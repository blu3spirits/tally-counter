Valid Usage:
```
./tallyCounter https://pastebin.com/id-of-template
./tallyCounter -u https://pastebin.com/id-of-template
./tallyCounter --url https://pastebin.com/id-of-template
```

This is intended to by used in conjuction with the DireWolf20 mod
"Building Gadgets". It should pull out the item names and amount of a given item.