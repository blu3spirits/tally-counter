""" Tally Counter

Usage:
    tallyCounter.py         <url>
    tallyCounter.py -u      <url>
    tallyCounter.py --url   <url>

Options:
    -h --help       Show this screen
    -v --version    Show version
    -u --url        Set the url
"""
from docopt import docopt

import json, re, requests
from collections import Counter
from bs4 import BeautifulSoup

class MaterialTally(object):
    def __init__(self, url):
        self.url = url
        self.html = self._getRawText()
        self.soup = BeautifulSoup(self.html, 'html.parser')

    def _getRawText(self):
        try:
            return requests.get(self.url).text
        except requests.exceptions.MissingSchema as err:
            print("URL is malformed: {}".format(err))
            exit(1)

    def _getData(self):
        return self._getRawPaste()

    def _getRawPaste(self):
        soup = self.soup
        raw = soup.find("textarea", {"id", "paste_code"})
        return raw.text

    def _getCount(self):
        data = self._getData()
        indices = [m.start(0) for m in re.finditer('\[I;', data)]
        dataSet = data[indices[0]+3:indices[1]-20].split(',')
        c = Counter(dataSet)
        return [(x,c[x]) for x in c]

    def _getTallies(self):
        data = self._getData()
        startLocation = data.find('mapIntState')
        loadedData = data[startLocation:len(data)]
        match = re.findall('(Name:")(\w+:\w+)', loadedData)
        generator = (x[1] for x in match)
        c = self._getCount()
        for k,v in enumerate(generator):
            print(c[k][1],v)
def main():
    arguments = docopt(__doc__, version="TallyCounter 1.0")
    if arguments['<url>'] or arguments['--url'] or arguments['-u']:
        tally = MaterialTally(arguments['<url>'])
        tally._getTallies()


if __name__ == '__main__':
    main()

